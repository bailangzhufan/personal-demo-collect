package com.algirthum;

import java.util.Arrays;

public class ShellSort extends SortBase {

    @Override
    public Integer[] sort(Integer[] a) {
        // TODO Auto-generated method stub
        print("init",a);
        Integer h = a.length;
        Integer temp = 0;
        while(h >= 1) {
            System.out.println(h);
            for(int i=h;i<a.length;i++) {
                System.out.println("i:"+i+"-----h:"+h);
                for(int j=i;j>=h && a[j] < a[j-h];j -= h) {
                    System.out.println("i:"+i+"-----j:"+j+"-----h:"+h);
                    temp = a[j];
                    a[j] = a[j-h];
                    a[j-h] = temp;
                    print("result",a);
                }
            }
            h /= 3;
        }
        print("result",a);
        return a;
    }
    
    public static void main(String[] args) {
        Integer[] a = {2,1,5,9,0,6,8,7,3};
        // (new ShellSort()).sort(a);

        Arrays.sort(a);
    }
}
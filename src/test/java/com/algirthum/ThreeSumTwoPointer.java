package com.algirthum;

import java.util.Arrays;

public class ThreeSumTwoPointer implements ThreeSum {

    @Override
    public int count(int[] nums) {
        int N = nums.length;
        int cnt = 0;
        //排序
        Arrays.sort(nums);
        //
        for (int i = 0; i < N - 2; i++) {
            int l = i + 1, h = N - 1, target = -nums[i];
            if (i > 0 && nums[i] == nums[i - 1]) continue;
            while (l < h) {
                int sum = nums[l] + nums[h];
                if (sum == target) {
                    cnt++;
                    while (l < h && nums[l] == nums[l + 1]) l++;
                    while (l < h && nums[h] == nums[h - 1]) h--;
                    l++;
                    h--;
                } else if (sum < target) {
                    l++;
                } else {
                    h--;
                }
            }
        }
        return cnt;
    }
    public static void main(String[] args) {
        ThreeSumTwoPointer threeSumTwoPointer = new ThreeSumTwoPointer();
        int [] a={1,2,3,-1,-2,-3,4,5,6};
        int count = threeSumTwoPointer.count(a);
        System.out.println(count);
    }

}
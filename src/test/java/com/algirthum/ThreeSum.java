package com.algirthum;

public interface ThreeSum {
    int count(int[] nums);
}
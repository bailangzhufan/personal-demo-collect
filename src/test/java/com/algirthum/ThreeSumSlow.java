package com.algirthum;

import java.util.Arrays;

public class ThreeSumSlow {

    public int count1(int[] nums) {
        int N = nums.length;
        int cnt = 0;
        for (int i = 0; i < N; i++) {
            for (int j = i + 1; j < N; j++) {
                for (int k = j + 1; k < N; k++) {
                    if (nums[i] + nums[j] + nums[k] == 0) {
                        System.out.println(nums[i] +" "+ nums[j] +" "+ nums[k]);
                        cnt++;
                    }
                }
            }
        }
        return cnt;
    }


    public int count2(int[] nums) {
        Arrays.sort(nums);
        int N = nums.length;
        int cnt = 0;
        for (int i = 0; i < N; i++) {
            for (int j = i + 1; j < N; j++) {
                int target = -nums[i] - nums[j];
                int index = BinarySearch.search(nums, target);
                // 应该注意这里的下标必须大于 j，否则会重复统计。
                if (index > j) {
                    cnt++;
                }
            }
        }
        return cnt;
    }


    public static void main(String[] args) {
        ThreeSumSlow threeSumSlow = new ThreeSumSlow();
        int [] a={1,2,3,-1,-2,-3,4,5,6};
        int count = threeSumSlow.count2(a);
        System.out.println(count);
    }
}


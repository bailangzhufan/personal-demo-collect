package com;

import lombok.Data;
import lombok.val;
import org.junit.Test;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author JFeng
 * @date 2018/9/30 9:25
 */

public class LomBookTest {
    @Test
    public void test01() {
        val setVar = new HashSet<String>();
        val listsVar = new   ArrayList<String>();
        val mapVar = new HashMap<String,   String>();

        //=>上面代码相当于如下：
        final Set<String> setVar2 = new   HashSet<>();
        final List<String> listsVar2 = new   ArrayList<>();
        final Map<String, String> maps2 =   new HashMap<>();
         checkNull("dsfsf");
    }
    @Test
    public void test02() {
         checkNull("dsfsf");
         checkNull("");
         checkNull(null);
    }
    @Test
    public void test03() {
        System.out.println(Runtime.getRuntime().availableProcessors());
    }

    private static void checkNull(@NotNull(message = "参数不能为空") String value) {
        if (value != null) {
            //方法内的代码相当于如下：
        } else {
            throw new NullPointerException("null");
        }
    }
}

package com.liulin.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @version: V1.0
 * @author: fendo
 * @className: Apple
 * @packageName: com.xxx.xxxx.xxxx.xxxx
 * @description: 苹果
 * @data: 2018-06-11 11:15  
 **/
@Data
public class AppleDTO {

    /**
     * 主键
     */
    private String id;

    /**
     * 名称
     */
    private String name;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 总数
     */
    private Long count;

    /**
     * 类别
     */
    private String type;
    private String haha;

    public AppleDTO() {

    }

    public AppleDTO(String id, String name, BigDecimal price, Long count, String type) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.count = count;
        this.type = type;
    }
}

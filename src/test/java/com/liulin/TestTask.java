package com.liulin;

import java.util.concurrent.*;

public class TestTask{
    private final ExecutorService exec; //线程池
    private final BlockingQueue<Future<Integer>> queue = new LinkedBlockingQueue<>();
    private final CountDownLatch startLock = new CountDownLatch(1); //启动门，当所有线程就绪时调用countDown
    private final CountDownLatch endLock; //结束门
    private final int nThread; //线程数量

    private volatile int count = 0;

    public TestTask(int nThread){
        this.nThread = nThread; //初始化线程数量
        exec = Executors.newFixedThreadPool(nThread); //创建线程池，线程池共有nThread个线程
        endLock = new CountDownLatch(nThread);  //设置结束门计数器，当一个线程结束时调用countDown
    }

    private class Task implements Callable<Integer>{
        @Override
        public Integer call() throws Exception {
            startLock.await(); //线程启动后调用await，当前线程阻塞，只有启动门计数器为0时当前线程才会往下执行
            try{
                for(int i = 0; i < 100; i++)
                    addCount();
                return count;
            }finally{
                endLock.countDown(); //线程执行完毕，结束门计数器减1
            }
        }
    }

    private synchronized void addCount() { count++; }

    public void init() throws InterruptedException, ExecutionException{
        for(int i = 0; i < nThread; i++){
            Future<Integer> future = exec.submit(new Task()); //将任务提交到线程池
            queue.add(future); //将Future实例添加至队列
        }
        startLock.countDown(); //所有任务添加完毕，启动门计数器减1，这时计数器为0，所有添加的任务开始执行
        endLock.await(); //主线程阻塞，直到所有线程执行完成
        for(Future<Integer> future : queue)
            System.out.println(future.get()); //获取Callable返回值
        System.out.println(count); //这里应当输出800
        exec.shutdown(); //关闭线程池
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException{
        TestTask task = new TestTask(8);
        task.init();
    }
}
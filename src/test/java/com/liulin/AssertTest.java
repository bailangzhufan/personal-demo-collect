package com.liulin;

import com.jfeng.test.Employee;
import org.apache.tomcat.jni.User;
import org.junit.Test;
import org.springframework.util.Base64Utils;

import java.lang.reflect.*;
import java.math.BigDecimal;

/**
 * @author JFeng
 * @date 2018/9/3 14:12
 */
public class AssertTest {
    @Test
    public void test01() {
        String s = Base64Utils.encodeToString("冯亮亮".getBytes());
        System.out.println(s);
        byte[] bytes = Base64Utils.decodeFromString(s);
        for (byte aByte : bytes) {
            System.err.print(aByte);
        }
        System.out.println(new String(bytes));
    }
    @Test
    public void test02() throws InvocationTargetException, IllegalAccessException {
        Field[] declaredFields = Employee.class.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            String s = declaredField.getName();
            AnnotatedType annotatedType = declaredField.getAnnotatedType();
            Type type = annotatedType.getType();
            String typeName = type.getTypeName();
            System.out.println(s);
            System.out.println(typeName);
        }
        Employee employee = new Employee("jfeng","1756666");
        Method[] declaredMethods = employee.getClass().getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            String name = declaredMethod.getName();
            System.out.println(name);
            if(name.startsWith("get")){
                Object invoke = declaredMethod.invoke(employee);
                System.out.println(invoke.toString());
            }
        }
    }
    @Test
    public void test03() {
        BigDecimal bigDecimal1 = new BigDecimal("20.11");
        BigDecimal bigDecimal2 = new BigDecimal("15.11");
        int i = bigDecimal1.compareTo(bigDecimal2);
    }
}

package com.liulin;

import com.google.common.collect.Maps;
import lombok.Data;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author JFeng
 * @date 2018/8/22 9:42
 */
public class StreamTest {
    @Test
    public  void test01()
    {
        List<Integer> filterLists = new ArrayList<>();
        for(int i=0;i<1000000;i++)
        {
            filterLists.add(i);
        }

        Date a = new Date();
        for(int j=0;j<1000000;j++)
        {
            System.out.println(filterLists.get(j));
        }
        Date b = new Date();

        Date c = new Date();
        filterLists.parallelStream().forEach(s -> System.out.println(s));
        Date d = new Date();

        long interval = b.getTime()-a.getTime();
        long interval2 = d.getTime()-c.getTime();
        System.out.println("两个时间相差1:"+interval);//6602
        System.out.println("两个时间相差2:"+interval2);//6824
    }
    @Test
    public  void test02(){
            String[] strs = { "aaa", "bbb", "ccc" };
            Arrays.stream(strs).map(str -> str.split("")).forEach(System.out::println);// Ljava.lang.String;@53d8d10a
            Arrays.stream(strs).map(str -> str.split("")).flatMap(Arrays::stream).forEach(System.out::println);// aaabbbccc
            Arrays.stream(strs).map(str -> str.split("")).flatMap(str -> Arrays.stream(str)).forEach(System.out::println);// aaabbbccc
    }

    /**
     * 将多个Stream连接成一个Stream，这时候不是用新值取代Stream的值，与map有所区别，这是重新生成一个Stream对象取而代之。
     */
    @Test
    public  void test03(){
        List<Integer> a=new ArrayList<>();
        a.add(1);
        a.add(2);
        List<Integer> b=new ArrayList<>();
        b.add(3);
        b.add(4);
        List<Integer> figures=Stream.of(a,b).flatMap(u->u.stream()).collect(Collectors.toList());
        figures.forEach(f->System.out.println(f));
    }
    @Test
    public void testMapAndFlatMap() {
        List<String> words = new ArrayList<String>();
        words.add("hello");
        words.add("word");

        //将words数组中的元素再按照字符拆分，然后字符去重，最终达到["h", "e", "l", "o", "w", "r", "d"]
        //如果使用map，是达不到直接转化成List<String>的结果
        List<String> stringList = words.stream()
                .flatMap(word -> Arrays.stream(word.split("")))
                .distinct()
                .collect(Collectors.toList());
        stringList.forEach(e -> System.out.println(e));
    }
    @Test
    public void test001() {
        List<Hosting> list = new ArrayList<>();
        list.add(new Hosting(1, "liquidweb.com", 80000));
        list.add(new Hosting(2, "linode.com", 90000));
        list.add(new Hosting(3, "digitalocean.com", 120000));
        list.add(new Hosting(4, "aws.amazon.com", 200000));
        list.add(new Hosting(5, "mkyong.com", 1));
        list.add(new Hosting(6, "linode.com", 100000)); // new line
        // key = name, value - websites , but the key 'linode' is duplicated!?
        Map<String, Long> result1 = list.stream().collect(
                Collectors.toMap(Hosting::getName, Hosting::getWebsites));
        System.out.println("Result 1 : " + result1);

    }
    @Test
    public void test002() {
        Map<String, String> map = new HashMap<>();
        map.put("1","dsdfsd");
        map.put("1","sssss");
    }


}
@Data
class Hosting {

    private int Id;
    private String name;
    private long websites;
    public Hosting(int id, String name, long websites) {
        Id = id;
        this.name = name;
        this.websites = websites;
    }

    //getters, setters and toString()
}

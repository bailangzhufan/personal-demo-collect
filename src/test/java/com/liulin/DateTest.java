package com.liulin;

import org.joda.time.*;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 * @author JFeng
 * @date 2018/8/20 10:51
 */

public class DateTest {


    private static void test03() {
        Date date1 = new Date(2018, 7, 16);
        Date date2 = new Date();
        Instant instant1 = date1.toInstant();
        Instant instant2 = date2.toInstant();
        ZoneId zoneId = ZoneId.systemDefault();

        // atZone()方法返回在指定时区从此Instant生成的ZonedDateTime。
        LocalDate localDate1 = instant1.atZone(zoneId).toLocalDate();
        LocalDate localDate2 = instant2.atZone(zoneId).toLocalDate();
        LocalDate date4 = LocalDate.of(2018, 1, 6);
        LocalDate localDate4 = date4.plusDays(1L);
        System.out.println(localDate1.toEpochDay() - localDate2.toEpochDay());
        System.out.println(localDate4.toString());
    }

    @Test
    public void test01() {
        // Now的表示
        LocalDateTime t1 = LocalDateTime.now();

        // 自定义时间
        LocalDateTime t2 = LocalDateTime.of(2000, 1, 1, 19, 0, 0);
        LocalDateTime t3 = LocalDateTime.parse("2007-12-03T10:15:30");
        LocalDateTime t4 = LocalDateTime.parse("2017-04-23 12:30", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        LocalDateTime t5 = LocalDateTime.parse("2018-01-08 00:00:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        boolean b = t4.isBefore(t5) && t4.isAfter(t3);
        System.out.println(b);
        if (t4.isBefore(t3) && t4.isAfter(t5)) {

        }

        // 时间的加减
        LocalDateTime t6 = t4.plusDays(5).plusHours(3);
        System.out.println(t6);
        LocalDateTime t7 = t2.plusDays(-5).plusHours(-3);

        // 算时间差
        Duration d1 = Duration.between(t6, t7);
        System.out.println(d1.toDays());
        Duration d2 = Duration.between(t7, t6);

        // 时间判等，千万不要用==
        LocalDateTime t8 = LocalDateTime.of(2000, 1, 1, 19, 0, 0);
        System.out.println(t2.isEqual(t8));

        // 时间判大小
        System.out.println(t1.isAfter(t2));

        // 转换成字符串
        System.out.println("" + t1 + t2 + t3 + t4 + t5);
        System.out.println(t1.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
    }

    public static void test02() {
        LocalDate dateFormer = LocalDate.parse("2018-08-30", DateTimeFormatter.ISO_DATE);
        LocalDate dateAfter = LocalDate.parse("2018-08-13", DateTimeFormatter.ISO_DATE);
        boolean after = dateAfter.isAfter(dateFormer);
        LocalDate now = LocalDate.now();
        System.out.println(dateFormer.toEpochDay() - dateAfter.toEpochDay());
    }

    public static void test04() {

        DateTime start = new DateTime(2015, 5, 4, 12, 20);
        DateTime end = new DateTime(2015, 5, 5, 12, 00);
        Interval interval = new Interval(start, end);
        System.out.println(interval.contains(new DateTime(2018, 5, 5, 11, 00)));

        // Duration duration=Duration.between();
        // System.out.println(duration.getStandardHours());

    }

    public static void test05() {
        DateTime start = new DateTime(2015, 5, 4, 12, 20);
        DateTime end = new DateTime(2015, 5, 5, 12, 00);
        System.out.println(Days.daysBetween(start, end).getDays());
        System.out.println(Hours.hoursBetween(start, end).getHours());
        System.out.println(Minutes.minutesBetween(start, end).getMinutes());
        System.out.println(Seconds.secondsBetween(start, end));


    }

    public static void test06() {
        LocalDateTime localDateTime = LocalDateTime.of(LocalDate.now(), LocalTime.MAX);
        System.out.println(localDateTime);

    }

    @Test
    public void test07() {
        LocalDateTime localDateTime = LocalDateTime.of(LocalDate.now(), LocalTime.MAX);
        LocalDate now = LocalDate.now();
        Date date1 = new Date(2018, 7, 16);
        boolean after = now.isBefore(date1.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        System.out.println(now + "" + after);
        System.out.println(localDateTime);
        System.out.println(localDateTime.plusDays(1L));

    }

    @Test
    public void test08() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(sdf.format(getStartTime()));
        System.out.println(sdf.format(getEndTime()));
    }

    private static Date getStartTime() {
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        return todayStart.getTime();
    }

    private static Date getEndTime() {
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.set(Calendar.HOUR, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);
        return todayEnd.getTime();
    }

}
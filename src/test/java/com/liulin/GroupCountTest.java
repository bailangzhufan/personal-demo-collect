package com.liulin;

import com.google.common.collect.Lists;
import com.liulin.entity.Apple;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author JFeng
 * @date 2018/8/26 15:00
 */
public class GroupCountTest {

    @Test
    public void test01() {

        List<Apple> appleList = Lists.newArrayList();
        Apple apple1 = new Apple();
        apple1.setId("1");
        apple1.setName("fendo1");
        apple1.setCount((long) 40);
        apple1.setType("1");
        apple1.setPrice(new BigDecimal(20));
        appleList.add(apple1);

        Apple apple2 = new Apple();
        apple2.setId("2");
        apple2.setName("fendo2");
        apple2.setCount((long) 10);
        apple2.setType("1");
        apple2.setPrice(new BigDecimal(20));
        appleList.add(apple2);

        Apple apple6 = new Apple();
        apple6.setId("6");
        apple6.setName("fendo6");
        apple6.setCount((long) 20);
        apple6.setType("1");
        apple6.setPrice(new BigDecimal(20));
        appleList.add(apple6);

        Apple apple3 = new Apple();
        apple3.setId("3");
        apple3.setName("fendo3");
        apple3.setCount((long) 10);
        apple3.setType("2");
        apple3.setPrice(new BigDecimal(20));
        appleList.add(apple3);

        Apple apple4 = new Apple();
        apple4.setId("4");
        apple4.setName("fendo4");
        apple4.setCount((long) 10);
        apple4.setType("3");
        apple4.setPrice(new BigDecimal(20));
        appleList.add(apple4);

        Apple apple5 = new Apple();
        apple5.setId("5");
        apple5.setName("fendo5");
        apple5.setCount((long) 10);
        apple5.setType("4");
        apple5.setPrice(new BigDecimal(20));
        appleList.add(apple5);

        Stream<Long> longStream = appleList.stream().map(Apple::getCount);

        //分组
        Map<String, List<Apple>> map = appleList.stream().collect(Collectors.groupingBy(Apple::getType, Collectors.collectingAndThen(Collectors.toList(), apples -> {
            System.out.println(apples.toString());
            return apples.stream().sorted((o1, o2) -> Math.toIntExact(o1.getCount() - o2.getCount())).collect(Collectors.toList());
        })));
        for (Map.Entry<String, List<Apple>> entry : map.entrySet()) {
            System.out.println("分组" + entry.getKey() + entry.getValue());
        }

        //分组求和
        Map<String, LongSummaryStatistics> collect = appleList.stream().collect(Collectors.groupingBy(Apple::getType, Collectors.summarizingLong(Apple::getCount)));
        for (Map.Entry<String, LongSummaryStatistics> entry : collect.entrySet()) {
            LongSummaryStatistics longSummaryStatistics = entry.getValue();
            System.out.println("----------------key----------------" + entry.getKey());
            System.out.println("求和:" + longSummaryStatistics.getSum());
            System.out.println("求平均" + longSummaryStatistics.getAverage());
            System.out.println("求最大:" + longSummaryStatistics.getMax());
            System.out.println("求最小:" + longSummaryStatistics.getMin());
            System.out.println("求总数:" + longSummaryStatistics.getCount());
        }

        appleList.stream().mapToLong(Apple::getCount).map(operand -> operand - 1);
        Function<Apple, Long> getCount = Apple::getCount;
        Runnable runnable = Apple::new;
        appleList.stream().collect(Collectors.groupingBy(getCount, Collectors.counting()));

        appleList.stream().collect(Collectors.toMap(Apple::getName, Apple::getPrice));
        appleList.stream().collect(Collectors.toMap(Apple::getName, apple -> {
            return apple.getPrice();
        }));
        //分组统计
        Map<Long, BigDecimal> collect1 = appleList.parallelStream().collect(Collectors.toMap(Apple::getCount, Apple::getPrice, BigDecimal::add));
        Map<Long, BigDecimal> collect2 = appleList.stream().collect(Collectors.toMap(Apple::getCount, Apple::getPrice, (bigDecimal, bigDecimal2) -> {
            return bigDecimal.add(bigDecimal2);
        }));
        Map<String, Long> collect3 = appleList.stream().collect(Collectors.groupingBy(Apple::getType, Collectors.counting()));
        Map<String, LongSummaryStatistics> collect4 = appleList.stream().collect(Collectors.groupingBy(Apple::getType, Collectors.summarizingLong(Apple::getCount)));
        collect3.forEach((aLong, bigDecimal) -> {
            System.out.println(aLong + "**********" + bigDecimal);
        });
    }


    @Test
    public void test02() {
        long uniqueWords = 0;
        try (Stream<String> lines = Files.lines(Paths.get("com/liulin/data.txt"), Charset.defaultCharset())) {
            uniqueWords = lines.flatMap(l -> Arrays.stream(l.split(" ")))
                    .distinct()
                    .count();
            System.out.println(lines);
            System.out.println(uniqueWords);
        } catch (IOException e) {
            //
        }
    }

    @Test
    public void test03() {
        String str = "100|66,55:200|567,90:102|43,54";

        // StringTokenizer strToke = new StringTokenizer(str, ":,|");// 默认不打印分隔符
        // StringTokenizer strToke=new StringTokenizer(str,":,|",true);//打印分隔符
        StringTokenizer strToke=new StringTokenizer(str,":,|'50'",false);//不打印分隔符
        while (strToke.hasMoreTokens()) {
            System.out.println(strToke.nextToken());
        }
    }
    @Test
    public void test04() {
        String str = "100|66,55:200|567,90:102|43,54";

        // StringTokenizer strToke = new StringTokenizer(str, ":,|");// 默认不打印分隔符
        // StringTokenizer strToke=new StringTokenizer(str,":,|",true);//打印分隔符
        StringTokenizer strToke=new StringTokenizer(str,":,|'50'",false);//不打印分隔符
        while (strToke.hasMoreTokens()) {
            System.out.println(strToke.nextToken());
        }
    }
}

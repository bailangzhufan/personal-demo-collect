package com.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * @author JFeng
 * @date 2018/10/11 9:52
 */
@Slf4j
public class CacheTest {
    @Test
    public void testCacheLoader() throws ExecutionException {
        LoadingCache<String, String> cahceBuilder = CacheBuilder
                .newBuilder()
                .build(new CacheLoader<String, String>() {
                    // 在load方法中定义value的加载方法；
                    // 这个方法要么返回已经缓存的值,要么使用 CacheLoader 向缓存原子地加载新值
                    @Override
                    public String load(String key) throws Exception {
                        String strProValue = "hello " + key + "!";
                        return strProValue;
                    }

                    // 默认情况下,对每个不在缓存中的键,getAll 方法会单独调用 CacheLoader.load 来加载缓存项。如果批量的加载比多个单独加载更高效,你可以重载 CacheLoader.loadAll 来利用这一点
                    @Override
                    public Map<String, String> loadAll(Iterable<? extends String> keys) throws Exception {
                        return super.loadAll(keys);
                    }
                });
        log.info("jerry value:" + cahceBuilder.apply("jerry"));
        log.info(("jerry value:" + cahceBuilder.get("jerry")));
        log.info(("peida value:" + cahceBuilder.get("peida")));
        log.info(("peida value:" + cahceBuilder.apply("peida")));
        log.info(("lisa value:" + cahceBuilder.apply("lisa")));
        cahceBuilder.put("harry", "ssdded");
        log.info(("harry value:" + cahceBuilder.get("harry")));
    }
}

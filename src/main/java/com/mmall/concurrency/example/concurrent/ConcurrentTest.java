package com.mmall.concurrency.example.concurrent;

/**
 * @author JFeng
 * @date 2018/11/23 9:06
 */
public class ConcurrentTest {

    public static void main(String[] args) {

        System.out.println(Integer.parseInt("0001111", 2) & 15);
        System.out.println(Integer.parseInt("0011111", 2) & 15);
        System.out.println(Integer.parseInt("0111111", 2) & 15);
        System.out.println(Integer.parseInt("1111111", 2) & 15);

    }
}

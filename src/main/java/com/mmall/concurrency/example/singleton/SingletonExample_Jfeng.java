package com.mmall.concurrency.example.singleton;

/**
 * @author JFeng
 * @date 2018/11/19 14:36
 */
public class SingletonExample_Jfeng {
    //私有 空参构造函数
    private SingletonExample_Jfeng() {
    }
    //获取单例对象
    public SingletonExample_Jfeng getInstance(){
        return ProduceInstance.INSTANCE.getSingleton();
    }

    //使用枚举类生成单例对象(通过枚举保证对象唯一)
    public enum  ProduceInstance {
        //生成唯一对象
        INSTANCE;

        private  SingletonExample_Jfeng singleton;

        //枚举构造
        ProduceInstance(){
            singleton=new SingletonExample_Jfeng();
        }
        //通过枚举对象get单例对象
        public SingletonExample_Jfeng getSingleton() {
            // this.singleton = new SingletonExample_Jfeng();
            return singleton;
        }
    }


}

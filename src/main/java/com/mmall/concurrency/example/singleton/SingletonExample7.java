package com.mmall.concurrency.example.singleton;

import com.mmall.concurrency.annoations.Recommend;
import com.mmall.concurrency.annoations.ThreadSafe;

/**
 * 枚举模式：最安全
 */
@ThreadSafe
@Recommend
public class SingletonExample7 {

    // 私有构造函数
    private SingletonExample7() {

    }

    public static SingletonExample7 getInstance() {
        return Singleton.INSTANCE.getSingleton();
    }

    private enum Singleton {
        INSTANCE;

        private SingletonExample7 singleton;

        // JVM保证这个方法绝对只调用一次 枚举调用空参构造  可以构建有参数构造
        // Singleton(SingletonExample7 singleton) { this.singleton = singleton; }
        Singleton() {
            singleton = new SingletonExample7();
        }

        public SingletonExample7 getSingleton() { return singleton; }
    }
}

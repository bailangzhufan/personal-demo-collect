package com.mmall.concurrency.example.commonUnsafe;

import com.mmall.concurrency.annoations.NotThreadSafe;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

@Slf4j
@NotThreadSafe
public class StringExample1 {

    // 请求总数
    public static int clientTotal = 5000;

    // 同时并发执行的线程数
    public static int threadTotal = 200;   //停车位

    public static StringBuilder stringBuilder = new StringBuilder();

    public static void main(String[] args) throws Exception {

        ExecutorService executorService = Executors.newCachedThreadPool();
        final Semaphore semaphore = new Semaphore(threadTotal);
        final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        for (int i = 0; i < clientTotal; i++) {
            executorService.execute(() -> {
                try {
                    //并发信号量 操场跑道5条即threadTotal=5   1 实现同步操作 上锁执行
                    semaphore.acquire();
                    update();
                    semaphore.release();
                } catch (Exception e) {
                    log.error("exception", e);
                }
                countDownLatch.countDown();//程序计数器递减
            });
        }
        //等所有任务结束销毁线程池
        countDownLatch.await();
        executorService.shutdown();
        log.info("size:{}", stringBuilder.length());
        //思考：
        // 在很多情况下，可能有多个线程需要访问数目很少的资源。假想在服务器上运行着若干个回答客户端请求的线程。
        // 这些线程需要连接到同一数据库，但任一时刻只能获得一定数目的数据库连接。你要怎样才能够有效地将这些固定数目的数据库连接分配给大量的线程？
        //    
        // 答：1.给方法加同步锁，保证同一时刻只能有一个人去调用此方法，其他所有线程排队等待，
        // 但是此种情况下即使你的数据库链接有10个，也始终只有一个处于使用状态。这样将会大大的浪费系统资源，而且系统的运行效率非常的低下。
        //     2.另外一种方法当然是使用信号量，通过信号量许可与数据库可用连接数相同的数目，将大大的提高效率和性能。
    }

    private static void update() {
        stringBuilder.append("1");
    }
}

//如上图所示,操作1和操作2做了重排序。程序执行时,线程A首先写标记变量flag,随后线程B读这个变量。
// 由于条件判断为真,线程B将读取变量a。此时,变量a还根本没有被线程A写入,在这里多线程程序的语义被重排序破坏了!
class ReorderExample {
    int a = 0;
    boolean flag = false;

    public void writer() {
        a = 1;//1
        flag = true;//2
    }

    public void reader() {
        if (flag) {//3
            int i = a * a;//4
        }
    }
}
//以处理器的猜测执行为例,执行线程B的处理器可以提前读取并计算a*a,然后把计算结果临时保存到一个名为重排序缓冲(reorder buffer ROB)的硬件缓存中。
//在单线程程序中,对存在控制依赖的操作重排序,
// 不会改变执行结果(这也是as-if-serial语义允许对存在控制依赖的操作做重排序的原因);
// 但在多线程程序中,对存在控制依赖的操作重排序,可能会改变程序的执行结果。

//时间片即CPU分配给各个程序的时间，每个线程被分配一个时间段，称作它的时间片，
// 即该进程允许运行的时间，使各个程序从表面上看是同时进行的。
// 如果在时间片结束时进程还在运行，则CPU将被剥夺并分配给另一个进程。如果进程在时间片结束前阻塞或结束，则CPU当即进行切换。
// 而不会造成CPU资源浪费。在宏观上：我们可以同时打开多个应用程序，每个程序并行不悖，同时运行。
// 但在微观上：由于只有一个CPU，一次只能处理程序要求的一部分，如何处理公平，一种方法就是引入时间片，每个程序轮流执行。
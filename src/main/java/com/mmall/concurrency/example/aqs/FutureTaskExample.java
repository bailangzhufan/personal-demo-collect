package com.mmall.concurrency.example.aqs;

import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.scope.ScopedProxyUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;

import java.time.LocalDateTime;
import java.util.concurrent.*;

@Slf4j
public class FutureTaskExample {

    // 请求总数
    public static int clientTotal = 2;

    // 同时并发执行的线程数
    public static int threadTotal = 200;

    public static int count = 0;

    public static final ExecutorService executorService = Executors.newCachedThreadPool();

    public static void main(String[] args) throws Exception {

        String result = ftExe();
        System.out.println(result);
    }

    private static String ftExe() throws InterruptedException, ExecutionException {
        final Semaphore semaphore = new Semaphore(threadTotal);
        final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        FutureTask<String> futureTask1 = new FutureTask<>(() -> {
            System.out.println("【futureTask1】开始任务"+LocalDateTime.now());
            log.info("do something in callable"+LocalDateTime.now());
            Thread.sleep(5000);
            System.out.println("【futureTask1】结束任务"+LocalDateTime.now());
            countDownLatch.countDown();
            return "Done1";
        });
        FutureTask<String> futureTask2 = new FutureTask<>(() -> {
            System.out.println("【futureTask2】开始任务"+LocalDateTime.now());
            log.info("do something in callable");
            Thread.sleep(10000);
            System.out.println("【futureTask2】结束任务"+LocalDateTime.now());
            countDownLatch.countDown();
            return "Done2";
        });
        executorService.execute(futureTask1);
        executorService.execute(futureTask2);
        //================非阻塞区域==============================
        countDownLatch.await();
        log.info("do something in main");
        //===================开始进行阻塞=========================
        String result2 = futureTask2.get();//阻塞方法
        System.out.println("阻塞2");
        String result1 = futureTask1.get();//阻塞方法
        System.out.println("阻塞1");
        log.info("result：{}", result1 + result2);
        return result1 + result2;
    }

    @Async
    public Future<String> sayHello1() throws InterruptedException {
        int thinking = 2;
        Thread.sleep(thinking * 1000);//网络连接中 。。。消息发送中。。。
        System.out.println("我爱你啊!");
        return new AsyncResult<String>("发送消息用了"+thinking+"秒");
    }
}

package com.mmall.concurrency.example.count;

import com.mmall.concurrency.annoations.NotThreadSafe;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

@Slf4j
@NotThreadSafe
public class CountExample1 {

    // 请求总数
    public static int clientTotal = 50;

    // 同时并发执行的线程数
    public static int threadTotal = 1;

    public static int count = 0;


    // 信号量Semaphore是一个并发工具类，用来控制可同时并发的线程数，其内部维护了一组虚拟许可，通过构造器指定许可的数量，
    // 每次线程执行操作时先通过acquire方法获得许可，执行完毕再通过release方法释放许可。如果无可用许可，那么acquire方法将一直阻塞，直到其它线程释放许可。
    //
    // 线程池用来控制实际工作的线程数量，通过线程复用的方式来减小内存开销。
    // 线程池可同时工作的线程数量是一定的，超过该数量的线程需进入线程队列等待，直到有可用的工作线程来执行任务。
    //
    // 所以使用Seamphore，你创建了多少线程，实际就会有多少线程进行执行，只是可同时执行的线程数量会受到限制。但使用线程池，不管你创建多少线程，实际可执行的线程数是一定的。

    //通过控制台容易观察，每次最多会打印2条***的记录。可以看出来总共创建的5条线程都执行完毕，5个线程对象互不相同。


    // Thread-0已获得许可
    // Thread-0已释放许可
    // Thread-1已获得许可
    // Thread-1已释放许可
    // Thread-2已获得许可
    // Thread-2已释放许可
    // Thread-3已获得许可
    // Thread-3已释放许可
    // Thread-4已获得许可
    // Thread-4已释放许可
    //
    // 可以看出，任何一个线程在释放许可之前，其它线程都拿不到许可。这样当前线程必须执行完毕，其它线程才可执行。这样就实现了互斥。

    public static void main(String[] args) throws Exception {
        ExecutorService executorService = Executors.newCachedThreadPool();
        final Semaphore semaphore = new Semaphore(threadTotal);
        final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        for (int i = 0; i < clientTotal ; i++) {
            executorService.execute(() -> {
                try {
                    semaphore.acquire();
                    Thread.sleep(1000L);
                    System.out.println(semaphore.availablePermits());
                    add();
                    semaphore.release();
                    System.out.println(semaphore.availablePermits());
                } catch (Exception e) {
                    log.error("exception", e);
                }
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
        executorService.shutdown();
        log.info("count:{}", count);
    }

    private static void add() {
        count++;
    }
}

package com.algrthim;

/**
 * 递归问题特征
 * <p>
 * Definition for a binary tree node.
 */

public class Solution {

    private static Boolean isBalanced = true;


    public static void main(String[] args) {
        TreeNode treeNode = new TreeNode(3);

        TreeNode left1 = new TreeNode(27);
        TreeNode right1 = new TreeNode(20);
        treeNode.setLeft(left1);
        treeNode.setRight(right1);

        right1.setLeft(new TreeNode(15));
        right1.setRight(new TreeNode(7));


        //测试一 查找树的层数和对称性
        // int maxDepth = maxDepth(treeNode);
        // System.out.println(maxDepth);
        // System.out.println(isBalanced);

        //测试二 翻转树
        TreeNode treeNodeInvert = invertTree(treeNode);
        System.out.println(treeNodeInvert);

        int sum = pathSum(treeNode, 30);
        System.out.println(sum);

    }

    /**
     * 找到TreeNode root挂载的子节点层数
     *
     * @param root
     * @return
     */
    public static int maxDepth(TreeNode root) {
        // 递归终止条件 找到叶子节点
        if (root == null) {
            return 0;
        } else {
            int left = maxDepth(root.left);
            int right = maxDepth(root.right);
            if (Math.abs(left - right) > 0) {
                isBalanced = false;
            }
            return 1 + Math.max(left, right);
        }
    }


    public static TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return null;
        } else {
            TreeNode left = root.left;  // 后面的操作会改变 left 指针，因此先保存下来
            root.left = invertTree(root.right);
            root.right = invertTree(left);
            return root;
        }
    }

    /**
     * 重合两棵树的值 （对应位置相加）
     *
     * @param t1
     * @param t2
     * @return
     */
    public static TreeNode mergeTrees(TreeNode t1, TreeNode t2) {
        //两棵树都不存在
        if (t1 == null && t2 == null) {
            return null;
        } else {
            //一棵树都存在
            if (t1 == null) {
                return t2;
            } else if (t2 == null) {
                return t1;
            }
            //两棵树都存在
            TreeNode root = new TreeNode(t1.val + t2.val);
            root.left = mergeTrees(t1.left, t2.left);
            root.right = mergeTrees(t1.right, t2.right);
            return root;
        }
    }

    /**
     * 路径求和找到指定sum值
     *
     * @param root
     * @param sum
     * @return
     */
    public static boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        } else if (root.left == null && root.right == null && root.val == sum) {
            return true;
        } else {
            //只要存在一支就是true
            return hasPathSum(root.left, sum - root.val) || hasPathSum(root.right, sum - root.val);
        }
    }

    /*
     * 统计路径和等于一个数的路径数量
     */
    public static int pathSum(TreeNode root, int sum) {
        if (root == null) return 0;
        int ret = pathSumStartWithRoot(root, sum) + pathSum(root.left, sum) + pathSum(root.right, sum);
        return ret;
    }
     // 3 12
     // 3 12                                               9 12                      20 12
     //     9 9             20 17
     //     ret=1           15 -3      7 10
     //                     null x    null y
    public static int pathSumStartWithRoot(TreeNode root, int sum) {
        if (root == null) return 0;
        int ret = 0;
        if (root.val == sum) ret++;
        ret += pathSumStartWithRoot(root.left, sum - root.val) + pathSumStartWithRoot(root.right, sum - root.val);
        return ret;
    }
}
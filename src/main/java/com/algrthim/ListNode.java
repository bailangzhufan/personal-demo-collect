package com.algrthim;

import com.sun.media.sound.SoftTuning;
import lombok.Data;

@Data
public class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
    }


    /**
     * 有序链表去重  从末端开始两两之间进行比较
     * @param head
     * @return
     */
    public static ListNode deleteDuplicates(ListNode head) {
        if (head == null || head.next == null){ return head;}
        else {
            //重新赋值next对象
            head.next = deleteDuplicates(head.next);
            return head.val == head.next.val ? head.next : head;}
    }

    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null) return l2;
        if (l2 == null) return l1;
        if (l1.val < l2.val) {
            l1.next = mergeTwoLists(l1.next, l2);
            return l1;
        } else {
            l2.next = mergeTwoLists(l1, l2.next);
            return l2;
        }
    }

    public static void main(String[] args) {
        ListNode listNode1 = new ListNode(1);
        ListNode listNode2= new ListNode(1);
        ListNode listNode3 = new ListNode(2);
        ListNode listNode4 = new ListNode(3);
        ListNode listNode5 = new ListNode(3);
        listNode1.setNext(listNode2);
        listNode2.setNext(listNode3);
        listNode3.setNext(listNode4);
        listNode4.setNext(listNode5);

        ListNode listNode = deleteDuplicates(listNode1);
        System.out.println(listNode);


        ListNode listNode6 = mergeTwoLists(listNode1, listNode2);
        System.out.println(listNode6);
    }
}

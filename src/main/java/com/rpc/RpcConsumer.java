/* 
 * Copyright 2011 Alibaba.com All right reserved. This software is the 
 * confidential and proprietary information of Alibaba.com ("Confidential 
 * Information"). You shall not disclose such Confidential Information and shall 
 * use it only in accordance with the terms of the license agreement you entered 
 * into with Alibaba.com. 
 */
package com.rpc;

import org.apache.poi.ss.formula.functions.T;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.Socket;

/**
 * RpcConsumer 
 *  
 * @author william.liangf 
 */  
public class RpcConsumer {  
      
    public static void main(String[] args) throws Exception {
        //取得代理对象 Proxy使用增强功能进行socket请求   Consumer中需要引入服务端接口类
        HelloService service = refer(HelloService.class, "127.0.0.1", 1234);
        // for (int i = 0; i < Integer.MAX_VALUE; i ++) {
            //每次调用多需要完成 TCP 三次握手
            String hello = service.hello("World" + 0);
            System.out.println(hello);  
            Thread.sleep(1000);  
        // }
    }
    /**
     * 引用服务
     *
     * @param <T> 接口泛型
     * @param interfaceClass 接口类型
     * @param host 服务器主机名
     * @param port 服务器端口
     * @return 远程服务
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public static <T> T refer(final Class<T> interfaceClass, final String host, final int port) throws Exception {
        if (interfaceClass == null)
            throw new IllegalArgumentException("Interface class == null");
        if (! interfaceClass.isInterface())
            throw new IllegalArgumentException("The " + interfaceClass.getName() + " must be interface class!");
        if (host == null || host.length() == 0)
            throw new IllegalArgumentException("Host == null!");
        if (port <= 0 || port > 65535)
            throw new IllegalArgumentException("Invalid port " + port);
        System.out.println("Get remote service " + interfaceClass.getName() + " from server " + host + ":" + port);
        return (T) Proxy.newProxyInstance(
                interfaceClass.getClassLoader(),
                new Class<?>[] {interfaceClass},

                new InvocationHandler() {
                    public Object invoke(Object proxy, Method method, Object[] arguments) throws Throwable {
                        System.out.println("service.hello()方法调用执行");
                //1.建立长连接
                Socket socket = new Socket(host, port);
                //连接建立成功
                        System.out.println("连接建立成功");
                // 调用outputstream发送请求
                try {
                    //装饰者模式 底层封装实际的outputstream
                    ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
                    try {
                        //方法名
                        output.writeUTF(method.getName());
                        //参数类型
                        output.writeObject(method.getParameterTypes());
                        //参数
                        output.writeObject(arguments);
                        System.out.println("执行调用"+method.getName()+arguments);
                        // 调用inputstream接收响应
                        ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
                        try {
                            Object result = input.readObject();
                            if (result instanceof Throwable) {
                                throw (Throwable) result;
                            }
                            return result;
                        } finally {
                            input.close();
                        }
                    } finally {
                        output.close();
                    }
                } finally {
                    socket.close();
                }
            }
        });
    }

      
}  
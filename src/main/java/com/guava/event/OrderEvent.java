package com.guava.event;

import lombok.Data;

//Guava 发布-订阅模式中传递的事件,是一个普通的POJO类
@Data
public class OrderEvent {  //事件
   private String message;
   private String name;

    public OrderEvent(String name) {
        this.name = name;
    }
}

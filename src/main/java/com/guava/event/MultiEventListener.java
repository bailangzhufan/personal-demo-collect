package com.guava.event;

import com.google.common.eventbus.Subscribe;

public class MultiEventListener {

   @Subscribe
   public void listen(OrderEvent event){
       System.out.println("receive msg 02: "+event.getMessage());
   }

   @Subscribe
   public void listen(String message){
       System.out.println("receive msg 02: "+message);
   }
}
package com.serverLoader;
/**
 * Created by laibao
 */
public class Dog implements Animal {
    @Override
    public void eat() {
        System.out.println("Dog eating...");
    }
}
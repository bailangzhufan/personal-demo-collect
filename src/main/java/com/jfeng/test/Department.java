package com.jfeng.test;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
@Data
public class Department {
    @NotNull
    @Valid
    private List<Employee> employees;
    @NotNull
    private String name;

    public Department(String name, List<Employee> employees) {
        this.employees = employees;
        this.name = name;
    }
}
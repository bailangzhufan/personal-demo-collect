package com.jfeng.test;

import jdk.internal.util.xml.impl.Input;
import org.hibernate.validator.internal.util.privilegedactions.GetClassLoader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;


public class CalendarDemo {
    Calendar calendar = null;

    public void test() {
        calendar = Calendar.getInstance();
    }

    // 基本用法，获取年月日时分秒星期
    public void test1() {
        // 获取年
        int year = calendar.get(Calendar.YEAR);

        // 获取月，这里需要需要月份的范围为0~11，因此获取月份的时候需要+1才是当前月份值
        int month = calendar.get(Calendar.MONTH) + 1;

        // 获取日
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        // 获取时
        int hour = calendar.get(Calendar.HOUR);
        // int hour = calendar.get(Calendar.HOUR_OF_DAY); // 24小时表示

        // 获取分
        int minute = calendar.get(Calendar.MINUTE);

        // 获取秒
        int second = calendar.get(Calendar.SECOND);

        // 星期，英语国家星期从星期日开始计算
        int weekday = calendar.get(Calendar.DAY_OF_WEEK);

        System.out.println("现在是" + year + "年" + month + "月" + day + "日" + hour
                + "时" + minute + "分" + second + "秒" + "星期" + weekday);
    }

    // 一年后的今天
    public void test2() {
        // 同理换成下个月的今天calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.YEAR, 1);

        // 获取年
        int year = calendar.get(Calendar.YEAR);

        // 获取月
        int month = calendar.get(Calendar.MONTH) + 1;

        // 获取日
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        System.out.println("一年后的今天：" + year + "年" + month + "月" + day + "日");
    }

    // 获取任意一个月的最后一天
    public void test3() {
        // 假设求6月的最后一天
        int currentMonth = 6;
        // 先求出7月份的第一天，实际中这里6为外部传递进来的currentMonth变量
        // 1
        calendar.set(calendar.get(Calendar.YEAR), currentMonth, 1);

        calendar.add(Calendar.DATE, -1);

        // 获取日
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        System.out.println("6月份的最后一天为" + day + "号");
    }

    // 设置日期
    public static void test4() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2000);
        System.out.println("现在是" + calendar.get(Calendar.YEAR) + "年");

        calendar.set(2008, 8, 8);
        // 获取年
        int year = calendar.get(Calendar.YEAR);

        // 获取月
        int month = calendar.get(Calendar.MONTH);

        // 获取日
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        System.out.println("现在是" + year + "年" + month + "月" + day + "日");
    }

    public static void test03() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        Calendar calendar = Calendar.getInstance();
        // calendar.set(calendar.get(Calendar.YEAR),2,0);
        // calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        System.out.println(calendar.getTime());
        calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 6);
        System.out.println(calendar.getTime());
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        if (true) {
            //上周
            cal.setFirstDayOfWeek(Calendar.MONDAY);

            //设置当前时间为上周的今天
            cal.setTime(toDate("20190101", "yyyyMMdd"));
            cal.add(Calendar.DAY_OF_YEAR, -7);

            //上周周一时间
            cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            System.out.println(cal.getTime());

            //上周周日时间
            cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
            String eTime = sdf.format(cal.getTime());
            System.out.println(cal.getTime());

        }
    }

    public static Date toDate(LocalDate localDate) {
        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime zdt = localDate.atStartOfDay(zoneId);

        Date date = Date.from(zdt.toInstant());

        return date;
    }

    public static Date toDate(String date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public  void testxxx() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(toDate("20190102", "yyyyMMdd"));
        //上周
        cal.add(Calendar.DAY_OF_YEAR,-1);
        System.out.println(cal.getTime());
        //设置当前时间为上周的今天
        cal.add(Calendar.DAY_OF_YEAR, -7);

        //上周周一时间
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        System.out.println(cal.getTime());

        //上周周日时间
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        System.out.println(cal.getTime());

    }



}
package com.jfeng.test;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
@Data
public class Employee {
    @NotNull
    private String name;
    @Pattern(regexp = "\\d{3}-\\d{3}-\\d{4}")
    private String phone;

    public Employee(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }
}
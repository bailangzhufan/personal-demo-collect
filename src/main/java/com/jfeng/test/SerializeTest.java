package com.jfeng.test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author JFeng
 * @date 2019/4/2 18:02
 */
public class SerializeTest {
    public static void main(String[] args) {
        User user = new User();
        GsonBuilder gsonBuilder = new GsonBuilder();
        // gsonBuilder.registerTypeAdapterFactory();
        Gson gson = gsonBuilder.create();
        System.out.println(gson.toJson(user));
    }
}

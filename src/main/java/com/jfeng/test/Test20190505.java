package com.jfeng.test;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author JFeng
 * @date 2019/5/5 9:30
 */
public class Test20190505 {
    public static void main(String[] args) {
        Date date = new Date(1565539200000L);
        BigDecimal bigDecimal = new BigDecimal("20.55");
        System.out.println(date);
        System.out.println(bigDecimal);

        System.out.println("10308065".hashCode() % 1024);
        System.out.println("10308065".hashCode() % 8192);
        System.out.println( "KXTEST".hashCode() % 1024);
        System.out.println("KXTEST".hashCode() % 8192);
        System.out.println("YBBTEST".hashCode() % 1024);
        System.out.println("YBBTEST".hashCode() % 8192);
    }
}

package com.jfeng.test;

import javax.validation.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class ValidationDemo5 {
    public static void main(String[] args) throws ParseException {
        Employee e1 = new Employee(null, "333333");
        Employee e2 = new Employee("Jake", "abc");
        List<Employee> employees = Arrays.asList(e1, e2);


        Department dept = new Department("Admin", Arrays.asList(e1, e2));

        Validator validator = createValidator();
        Set<ConstraintViolation<Department>> violations = validator.validate(dept);
        if (violations.size() == 0) {
            System.out.println("No violations.");
        } else {
            System.out.printf("%s violations:%n", violations.size());
            violations.stream()
                    .forEach(ValidationDemo5::printError);
        }
    }

    private static void printError(ConstraintViolation<?> violation) {
        System.out.println(violation.getPropertyPath()
                + " " + violation.getMessage());
    }

    public static Validator createValidator() {
        Configuration<?> config = Validation.byDefaultProvider().configure();
        ValidatorFactory factory = config.buildValidatorFactory();
        Validator validator = factory.getValidator();
        factory.close();
        return validator;
    }
}
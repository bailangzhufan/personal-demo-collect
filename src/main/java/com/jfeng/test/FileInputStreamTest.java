package com.jfeng.test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * @author JFeng
 * @date 2019/3/4 9:15
 */
public class FileInputStreamTest {
    public static void main(String[] args) throws IOException {
        FileInputStreamTest fileInputStreamTest = new FileInputStreamTest();
        ClassLoader classLoader = fileInputStreamTest.getClass().getClassLoader();
        URL resource = classLoader.getResource("solver/jfeng.xml");
        InputStream inputStream = resource.openStream();
        int read = inputStream.read();
    }
}

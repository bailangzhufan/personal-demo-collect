package com.jfeng.canal.serverLoader;

public interface IMyServiceLoader {

    String sayHello();

    String getName();
}
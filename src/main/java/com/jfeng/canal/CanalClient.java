package com.jfeng.canal;

import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

/**
 * @author JFeng
 * @date 2018/9/12 15:23
 */
@Slf4j
public class CanalClient {
    public static void main(String[] args) {
        String property = System.getProperty("config-dir");
        // D:\开源项目\demo_project\jfengTestProject01\src\main\resources\solver\config.properties
        Properties configProperties = getConfigProperties(property+"\\solver\\config.properties", false);
        System.out.println(configProperties);
    }

    private static Properties getConfigProperties(String confFilePath, boolean cp){
        Properties configProperties;
        configProperties = new Properties();
        InputStream is = null;
        String path = confFilePath;
        try{
            if(cp) {
                is = getResourceAsStream(confFilePath);
                path = getResource(confFilePath).toString();
            }else {
                is = new FileInputStream(confFilePath);
            }
            log.info("loading configruation from "+path);
            configProperties.load(is);
        }catch(IOException e){
            throw new RuntimeException("failed to load configuration from "+confFilePath);
        }finally {
            if(is != null) {
                try{is.close();}catch(IOException e) {log.error("failed to close input stream of "+path, e);}
            }
        }
        if(configProperties.isEmpty()){
            log.warn("nothing loaded from "+path);
        }
        return configProperties;
    }

    public static InputStream getResourceAsStream(String path){
        InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(path);
        if(is == null){
            is = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
        }
        if(is == null) {
            throw new RuntimeException("resouce file not found. "+path);
        }
        return is;
    }

    public static URL getResource(String path){
        URL url = ClassLoader.getSystemClassLoader().getResource(path);
        if(url == null){
            url = Thread.currentThread().getContextClassLoader().getResource(path);
        }
        if(url == null) {
            throw new RuntimeException("resouce file not found. "+path);
        }
        return url;
    }
}

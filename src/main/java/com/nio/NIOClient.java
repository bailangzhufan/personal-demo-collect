package com.nio;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class NIOClient {

    public static void main(String[] args) throws IOException {
        for (int i = 0; i < 10; i++) {
            Socket socket = new Socket("127.0.0.1", 8888);
            // 客户端建立连接----->>>>>>>>1554197582544
            OutputStream out = socket.getOutputStream();
            String s = "hello world";
            System.out.println("发送消息"+System.currentTimeMillis());
            out.write(s.getBytes());
            // 接收客户端请求----->>>>>>>>1554197638473
            out.close();
            //hello world


            // 每隔1秒 请求1次
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
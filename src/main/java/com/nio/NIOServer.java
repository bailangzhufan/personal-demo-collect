package com.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

// ServerSocketChannel----->>>>>selector   serverSocket  address
//NIO实现了IO多路复用的Ractor模型，一个线程Thread使用一个选择器Selector通过轮询的方式去监听多个通道Channel上的事件，找到IO事件已经到达的Channle执行
// 因为创建和切换线程的开销很大，因此使用一个线程来处理多个事件而不是一个线程处理一个事件，具有更好的性能


public class NIOServer {

    public static void main(String[] args) throws IOException {
        // 通过调用Selector.open()方法创建一个Selector
        //isOpen() —— 判断Selector是否处于打开状态。Selector对象创建后就处于打开状态了
        // close() —— 当调用了Selector对象的close()方法，就进入关闭状态.。用完Selector后调用其close()方法会关闭该Selector，且使注册到该Selector上的所有SelectionKey实例无效。通道本身并不会关闭
        Selector selector = Selector.open();

        /*
        java.nio.channels.Channel 接口：
            |--FileChannel  用于读取、写入、映射和操作文件的通道。
            |--SocketChannel  通过 TCP 读写网络中的数据。
            |--ServerSocketChannel 可以监听新进来的 TCP 连接，对每一个新进来的连接都会创建一个 SocketChannel。
            |--DatagramChannel  通过 UDP 读写网络中的数据通道。
       */

        ServerSocketChannel ssChannel = ServerSocketChannel.open();

        /**
         * 注意：
         * （1）通道必须配置为 非阻塞 模式，否则使用选择器没有任何意义
         * 因为如果通道在某个事件上被阻塞，那么服务器就不能响应其他事件，必须等待这个事件处理完毕才能去处理其他事件
         * （2）通道主要分为两大类：文件（File）通道 和 套接字（Socket）通道
         *   涉及的类主要有FileChannel 类 和 三个Socket通道类：SocketChannel、ServerSocketChannel和DatagramChannel
         *   应该注意的是：只有套接字Channel才能配置为 非阻塞，而FileChannel不能，为FileChannel配置为 非阻塞没有意义
         */
        ssChannel.configureBlocking(false);

        // 将channel注册到selector上
        // 只要ServerSocketChannel及SocketChannel向Selector注册了特定的事件，Selector就会监控这些事件是否发生。
        // SelectableChannel的register()方法返回一个SelectionKey对象，该对象是用于跟踪这些被注册事件的句柄。
        // 当register()方法执行时，新建一个SelectioKey，并把它加入Selector的all-keys集合中。
        // 如果关闭了与SelectionKey对象关联的Channel对象，或者调用了SelectionKey对象的cancel方法，这个SelectionKey对象就会被加入到cancelled-keys集合中，表示这个SelectionKey对象已经被取消。

        /**
         * 注册的具体事件：主要有一下几类：    权限定义不用
         *      SelectionKey.OP_CONNECT  --- 连接就绪事件，表示客户端与服务器的连接已经建立成功
         *      SelectionKey.OP_ACCEPT   --- 接收连接事件，表示服务器监听到了客户连接，服务器可以接收这个连接了
         *      SelectionKey.OP_READ     --- 读 就绪事件，表示通道中已经有了可读的数据，可以执行读操作了
         *      SelectionKey.OP_WRITE    --- 写 就绪事件，表示已经可以向通道写数据了
         */
        ssChannel.register(selector, SelectionKey.OP_ACCEPT);

        //          selector.keys();    selector.selectedKeys();
        // 一个Selector对象会包含3种类型的SelectionKey集合：
        //         all-keys集合 —— 当前所有向Selector注册的SelectionKey的集合，Selector的keys()方法返回该集合
        //         selected-keys集合 —— 相关事件已经被Selector捕获的SelectionKey的集合，Selector的selectedKeys()方法返回该集合
        //         cancelled-keys集合 —— 已经被取消的SelectionKey的集合，Selector没有提供访问这种集合的方法


        ServerSocket serverSocket = ssChannel.socket();
        InetSocketAddress address = new InetSocketAddress("127.0.0.1", 8888);
        serverSocket.bind(address);

        while (true) {
            //死循环执行select   阻塞到至少有一个通道在你注册的选择器上就绪了
            //在执行Selector的select()方法时，如果与SelectionKey相关的事件发生了，这个SelectionKey就被加入到selected-keys集合中，
            /* select() —— 阻塞到至少有一个通道在你注册的事件上就绪了
               select(long timeout) —— 和select()一样，除了最长会阻塞timeout毫秒
               selectNow() —— 不会阻塞，不管什么通道就绪都立刻返回；此方法执行非阻塞的选择操作，如果自从上一次选择操作后，没有通道变成可选择的，则此方法直接返回0
               select()方法返回的Int值表示多少通道就绪。*/
            selector.select();
            // System.out.println( selector.select() + "条通道就绪");

            // 程序直接调用selected-keys集合的remove()方法，或者调用它的iterator的remove()方法，都可以从selected-keys集合中删除一个SelectionKey对象。
            Set<SelectionKey> keys = selector.selectedKeys();
            Iterator<SelectionKey> keyIterator = keys.iterator();
            //遍历keys  ---->>>>>  (SelectionKey) 遍历这个已选择的集合来访问就绪的通道   来检测channel中什么事件或操作已经就绪 key.isAcceptable()  key.isReadable()
            while (keyIterator.hasNext()) {

                SelectionKey key = keyIterator.next();
                //todo   服务器监听到了一个客户端连接---->>>>>Tests whether this key's channel is ready to accept a new socket connection.
                if (key.isAcceptable()) {
                    System.out.println("客户端建立连接----->>>>>>>>"+System.currentTimeMillis());
                    ServerSocketChannel ssChannel1 = (ServerSocketChannel) key.channel();

                    // 服务器会为每个新连接创建一个 SocketChannel
                    SocketChannel sChannel = ssChannel1.accept();
                    sChannel.configureBlocking(false);

                    // 这个新连接主要用于从客户端读取数据
                    sChannel.register(selector, SelectionKey.OP_READ);

                }
                //todo  ---->>>>> Tests whether this key's channel is ready for reading.
                else if (key.isReadable()) {
                    System.out.println("接收客户端请求----->>>>>>>>"+System.currentTimeMillis());
                    SocketChannel sChannel = (SocketChannel) key.channel();
                    System.out.println(readDataFromSocketChannel(sChannel));
                    sChannel.close();
                }
                /**
                 * 每次迭代末尾的remove()调用，Selector不会自己从已选择的SelectionKey集合中
                 * 移除SelectionKey实例的，必须在处理完通道时自己移除   selectedKeys（相关事件已经被Selector捕获的SelectionKey的集合）
                 */
                keyIterator.remove();
            }
        }
    }

    private static String readDataFromSocketChannel(SocketChannel sChannel) throws IOException {

        ByteBuffer buffer = ByteBuffer.allocate(1024);
        StringBuilder data = new StringBuilder();

        while (true) {

            buffer.clear();
            int n = sChannel.read(buffer);
            if (n == -1) {
                break;
            }
            buffer.flip();
            int limit = buffer.limit();
            char[] dst = new char[limit];
            for (int i = 0; i < limit; i++) {
                dst[i] = (char) buffer.get(i);
            }
            data.append(dst);
            buffer.clear();
        }
        return data.toString();
    }
}
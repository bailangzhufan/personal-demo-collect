package com.Netty;


import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.concurrent.TimeUnit;

/**
 * 用于处理请求数据
 */
public class ServerHandler extends ChannelInboundHandlerAdapter {


    /*ChannelHandler是一个接口，处理I / O事件或拦截I / O操作，并将其转发到其ChannelPipeline(业务处理链)中的下一个处理程序。*/

    /*ChannelHandlerContext保存Channel相关的所有上下文信息，同时关联一个ChannelHandler对象*/

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {

        // 如何符合约定，则调用本地方法，返回数据
        if (msg.toString().startsWith("https://")) {
            String result = "成功接收请求" + msg.toString();

            ctx.channel().eventLoop().execute(new Runnable() {
                @Override
                public void run() {
                    //自定义任务执行
                    System.out.println("自定义任务执行....");
                }
            });

            ctx.channel().eventLoop().schedule(new Runnable() {
                @Override
                public void run() {
                    //自定义定时任务执行
                    System.out.println("自定义定时任务执行....");
                }
            }, 60, TimeUnit.SECONDS);

            ctx.writeAndFlush(result);
        }

    }
}

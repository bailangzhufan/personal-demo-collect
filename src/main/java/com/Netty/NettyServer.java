package com.Netty;


import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.util.Date;

/**
 * Netty针对NIO的封装
 * @author JFeng
 * @date 2019/4/4 16:34
 */
public class NettyServer {
    public static void main(String[] args) {
        // Netty基于Selector对象实现I/O多路复用，通过 Selector, 一个线程可以监听多个连接的Channel事件,
        // 当向一个Selector中注册Channel 后，Selector 内部的机制就可以自动不断地查询(select)
        // 这些注册的Channel是否有已就绪的I/O事件(例如可读, 可写, 网络连接完成等)，这样程序就可以很简单地使用一个线程高效地管理多个 Channel 。
        //备注:-----》》》》联想NIO中 Selector selector = Selector.open();   ssChannel.register(selector, SelectionKey.OP_ACCEPT);

        /*NioEventLoop中维护了一个线程和任务队列，支持异步提交执行任务，线程启动时会调用NioEventLoop的run方法，执行I/O任务和非I/O任务：
            I/O任务  nio底层
            即selectionKey中ready的事件，如accept、connect、read、write等，由processSelectedKeys方法触发。
            非IO任务
            添加到taskQueue中的任务，如register0、bind0等任务，由runAllTasks方法触发。
            两种任务的执行时间比由变量ioRatio控制，默认为50，则表示允许非IO任务执行的时间与IO任务的执行时间相等。*/

        /*NioEventLoopGroup，主要管理eventLoop的生命周期，可以理解为一个线程池，内部维护了一组线程，
        每个线程(NioEventLoop)负责处理多个Channel上的事件，而一个Channel只对应于一个线程。*/

        // 创建mainReactor
        NioEventLoopGroup boosGroup = new NioEventLoopGroup();
        // 创建工作线程组
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();


        /*Bootstrap意思是引导，一个Netty应用通常由一个Bootstrap开始，主要作用是配置整个Netty程序，串联各个组件，
        Netty中Bootstrap类是客户端程序的启动引导类，ServerBootstrap是服务端启动引导类。*/

        /*不同协议、不同的阻塞类型的连接都有不同的 Channel 类型与之对应，下面是一些常用的 Channel 类型
      >>>>>>>>  NioSocketChannel，异步的客户端 TCP Socket 连接
                NioServerSocketChannel，异步的服务器端 TCP Socket 连接
                NioDatagramChannel，异步的 UDP 连接
                NioSctpChannel，异步的客户端 Sctp 连接
                NioSctpServerChannel，异步的 Sctp 服务器端连接
                这些通道涵盖了 UDP 和 TCP网络 IO以及文件 IO.*/

        /*ChannelPipeline保存ChannelHandler的List，用于处理或拦截Channel的入站事件和出站操作。
        ChannelPipeline实现了一种高级形式的拦截过滤器模式，使用户可以完全控制事件的处理方式，以及Channel中各个的ChannelHandler如何相互交互。 */
        final ServerBootstrap serverBootstrap = new ServerBootstrap();
        serverBootstrap
                // 组装NioEventLoopGroup
                .group(boosGroup, workerGroup)
                // 设置channel类型为NIO类型
                .channel(NioServerSocketChannel.class)
                // 设置连接配置参数
                .option(ChannelOption.SO_BACKLOG, 1024)
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .childOption(ChannelOption.TCP_NODELAY, true)
                // 配置入站、出站事件handler
                .childHandler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel ch) {
                        ChannelPipeline pipeline = ch.pipeline();
                        // 配置入站、出站事件channel
                        pipeline.addLast(new StringDecoder());
                        pipeline.addLast(new StringEncoder());
                        pipeline.addLast(new ServerHandler());
                    }
                });

        // 绑定端口
        int port = 8080;
        /*正如前面介绍，在Netty中所有的IO操作都是异步的，不能立刻得知消息是否被正确处理，\
        但是可以过一会等它执行完成或者直接注册一个监听，具体的实现就是通过Future和ChannelFutures，
        他们可以注册一个监听，当操作执行成功或失败时监听会自动触发注册的监听事件。*/
        ChannelFuture channelFuture = serverBootstrap.bind(port);
        channelFuture.addListener(future -> {
            if (future.isSuccess()) {
                System.out.println(new Date() + ": 端口[" + port + "]绑定成功!");
            } else {
                System.err.println("端口[" + port + "]绑定失败!");
            }
        });
    }

}
